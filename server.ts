import { PrismaClient } from '@prisma/client'
import fastifyFormbody from '@fastify/formbody';
import fastify from 'fastify';

const prisma = new PrismaClient();


const app = fastify({logger:true})
app.register(require('@fastify/formbody'));


const PORT = 5000;


interface IByIdParam {
  id: string;
}


interface IBodyParam {
  title: string,
  authorID: string,
  categoryID: string
}


// Просмотр


app.get('/', async (request, reply) => {
  reply.send("Test");
});

app.get('/users', async(request, response) => {
  const users = await prisma.user.findMany()
  response.send(users);
})

app.get('/categories', async(request, response) => {
  const categories = await prisma.category.findMany()
  response.send(categories);
})


app.get('/articles', async(request, response) => {  
  const articles = await prisma.article.findMany()
  response.send(articles);
})

app.get<{Params: IByIdParam}>('/articles/:id', async(request, response) => {
  const { id } = request.params;
  const articles = await prisma.article.findUnique({
    where: {
      id: id
    }
  })
  response.send(articles);
})


// Создание

app.post<{ Body: IBodyParam }>('/articles', async(request, response) => {
  const {  title, authorID, categoryID } = request.body;
  const article = await prisma.article.create({
    data: {
      title: title,
      authorID: authorID,
      categoryID: categoryID
    }
  })
  response.send(article);
})

// Изменение

app.put<{ Params: IByIdParam, Body: IBodyParam }>
  ('/articles/:id', async(request, response) => {
  const { id } = request.params;
  const {  title } = request.body;
  const article = await prisma.article.update({
    where: {
      id: id
    },
    data: {
      title: title
    }
  })
  response.send(article);
})

// Удаление

app.delete<{Params: IByIdParam}>('/articles/:id', async(request, response) => {
  const { id } = request.params;
  const articles = await prisma.article.delete({
    where: {
      id: id
    }
  })
  response.send('Статья удалена');
})


app.listen({ port: PORT }, function (err, address) {
  if (err) {
    app.log.error(err)
    process.exit(1)
  }
})


