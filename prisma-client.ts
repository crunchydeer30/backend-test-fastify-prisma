import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

async function main() {
    
    const article = await prisma.article.update({
        where: {
            id: '2f1fa429-5fe5-422c-978e-abb737ed20bd'
        },
        data: {
            title: 'Edited',
        }
    })

    console.log(article);

}

main();